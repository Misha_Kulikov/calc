package Calc;
/**
 * Импортирую сканнер из пакетов java
 */

import java.util.Scanner;

/**
 * Это демокласс он создан для показа работы и выполнения
 * некоторых задач по типу : сложение , вычитания , деление
 * умножения , возведению в степень , остатка от деления
 */
public class CalcDemo {
    /**
     * Объявление метода сканнер
     */
    static Scanner scanner = new Scanner(System.in);

    /**
     * Ввод данных для расчёта,
     * использование методов,
     * и вывод ответа на экран
     */

    public static void main(String[] args) {

        String input = scanner.nextLine();

        String[] array = input.split(" ");

        double a = Double.parseDouble(array[0]);
        double b = Double.parseDouble(array[2]);

        switch (array[1]) {
            case "+":
                System.out.println(CalculatorSystem.amount(a, b));
                break;

            case "-":
                System.out.println(CalculatorSystem.difference(a, b));
                break;

            case "*":
                System.out.println(CalculatorSystem.multiplication(a, b));
                break;

            case "/":
                System.out.println(CalculatorSystem.division(a, b));
                break;

            case "%":
                System.out.println(CalculatorSystem.rest(a, b));
                break;

            case "^":
                System.out.println(CalculatorSystem.degree(a, (int) b));
                break;

            default:
                System.out.println("Ни один знак не подошёл");
        }
    }
}